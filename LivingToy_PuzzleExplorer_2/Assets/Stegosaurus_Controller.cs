﻿using UnityEngine;
using System.Collections;

public class Stegosaurus_Controller : MonoBehaviour {
    Animator anim;
    public GameObject targetTRex;
    public GameObject targetBox;
    public bool tRex_A;
    Vector3 rotationLast;
    SkinnedMeshRenderer render;
    public bool active;
    float speed = .75f;
    Vector3 rotationPre;
    Quaternion originalRotation;
    Quaternion newLocalRotation;
    Quaternion newWorldRotation;
    float prevAngle = 0;
    public bool turning = false;
    bool hurting = false;
    // Use this for initialization
    void Start () {
        render = GetComponentInChildren<SkinnedMeshRenderer>();
        anim = GetComponent<Animator>();

    }

    float angleChange;
    Quaternion lastQuat = new Quaternion();
    // Update is called once per frame
    void Update () {

        if (render.enabled) active = true;
        else active = false;
        prevAngle = transform.localRotation.y;
        float steps = speed * Time.deltaTime;
        targetBox.transform.position = targetTRex.transform.position;
        if (targetTRex.GetComponent<TRex_Controller>().active)
        {
            float angletTarget = angleBetween2CartesianPoints(transform.localPosition.x, transform.localPosition.z, targetBox.transform.localPosition.x, targetBox.transform.localPosition.z);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0f, angletTarget, 0f), steps);
            if (Mathf.Abs(transform.localRotation.eulerAngles.y - angletTarget) > 2f) turning = true;
            else turning = false;

            //attacking = (Vector3.Distance(transform.localPosition, targetBox.transform.localPosition) < 2f && !turning);
        }
        else turning = false;

        if (targetTRex.transform.GetComponent<TRex_Controller>().attacking) hurting = true;
        else hurting = false;
        //print(hurting);
        anim.SetBool("Turning", turning);
        anim.SetBool("Hurt", hurting);
    }
    public float angleBetween2CartesianPoints(float firstX, float firstY, float secondX, float secondY)
    {
        float angle = Mathf.Atan2((secondX - firstX), (secondY - firstY)) * 180 / Mathf.PI;
        if (angle < 0)
        {
            return (360 + angle);
        }
        else
        {
            return (angle);
        }
    }
}
