﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class RayCastFromCursor : MonoBehaviour
{
    public GameObject cardboardMainObject;
    // Use this for initialization
    void Start()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        //CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
    }

    // Update is called once per frame
    void Update()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        if (Cardboard.SDK.Triggered)
        {
            Ray ray;
            RaycastHit raycastHit;
            print("Mouse Down");
            if (cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled)
            {
                Physics.Raycast(transform.position, transform.forward, out raycastHit);
            }
            else
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Physics.Raycast(ray, out raycastHit);
            }
            if (raycastHit.collider != null)
            {
                print("Hit Some thing");
                if ((raycastHit.collider.tag == "T-Rex_A" || raycastHit.collider.tag == "T-Rex_B"))
                {
                    print("Hit T-Rex");
                    raycastHit.collider.transform.GetComponent<TRex_Controller>().Roar();
                }
                if (raycastHit.collider.tag == "VrToggle")
                {
                    ARVRToggle();
                }
            }

        }




        /*Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Ray spriteCursorRay = new Ray(transform.position, transform.forward);
        RaycastHit SpriteCursorRaycastHit;
        Physics.Raycast(spriteCursorRay, out SpriteCursorRaycastHit);
        if (SpriteCursorRaycastHit.collider != null)
        {
            if (Input.GetMouseButtonDown(0)&&(SpriteCursorRaycastHit.collider.tag == "T-Rex_A" || SpriteCursorRaycastHit.collider.tag == "T-Rex_B")){
                //print("Hit T-Rex");
                SpriteCursorRaycastHit.collider.transform.GetComponent<TRex_Controller>().Roar();
            }
        }*/
    }
    public void ARVRToggle()
    {
        cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled = !(cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled);
    }
}
