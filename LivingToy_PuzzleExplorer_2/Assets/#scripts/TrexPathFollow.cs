﻿using UnityEngine;
using System.Collections;

public class TrexPathFollow : MonoBehaviour {

	Vector3 current = new Vector3();
	Vector3 previous = new Vector3();
	float timer;
	Animator anim;
	AudioSource grunt;
	AudioSource roar;

	bool playGrunt = true;
	// Use this for initialization
	void Start () 
	{
		anim = GetComponentInChildren<Animator>();
		transform.localScale = new Vector3(-1, 1, 1);
		grunt = GetComponents<AudioSource>()[0];
		roar = GetComponents<AudioSource>()[1];

		Invoke("StartEnterPath", 5f);
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		Vector3 vel = (transform.position - previous) / Time.deltaTime;

		previous = transform.position;
//		if (vel.magnitude > 0f)
//			anim.SetBool("IsMoving", true);
//		else
//			anim.SetBool("IsMoving", false);

		if (vel != new Vector3(0,0,0))
			transform.rotation = Quaternion.LookRotation(vel);

		timer += Time.deltaTime;

		if (timer > 5 && playGrunt)
		{
			timer -= Random.Range(5f, 10f);
			grunt.pitch = Random.Range(.9f, 1.1f);
			grunt.Play();
		}

	
	}
	bool isAnimated = false;
	public void StartEnterPath()
	{

		if (!isAnimated)
		{

			iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("enter"), "time", 45f, "easeType", iTween.EaseType.linear, "orienttopath", true, "oncomplete", "StartLeavePath"));
			isAnimated = true;
			anim.SetBool("IsMoving", true);


		}


	}




	void StartLeavePath()
	{
		ParticleHandler[] handlers = GetComponentsInChildren<ParticleHandler>();
		for (int i = 0; i < handlers.Length; i++)
		{
			handlers[i].disable = true;
		}

		iTween.LookTo(gameObject, iTween.Hash("looktarget", iTweenPath.GetPath("leave"), "time", 2f, "oncomplete", "LeavePath", "delay", 14.5f));
		playGrunt = false;
		Invoke("RoarSound", 2f);
		anim.SetBool("IsMoving", false);
		//anim.Play("Roar");


	}

	void RoarSound()
	{
		roar.Play();
		playGrunt = true;
		timer = -3;
	}

	void LeavePath()
	{

		iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("leave"), "time", 25f, "easeType", iTween.EaseType.linear,  "orienttopath", true, "oncomplete", "EnableLoop"));

		anim.SetBool("IsMoving", true);

	}
	void EnableLoop()
	{
		isAnimated = false;
		Invoke("StartEnterPath", 5f);
	}

}
