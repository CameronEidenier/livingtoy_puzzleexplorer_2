﻿using UnityEngine;
using System.Collections;

public class SpinoPathFollow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("StartEnterPath", 3f);

	}

	Vector3 current = new Vector3();
	Vector3 previous = new Vector3();
	void FixedUpdate () 
	{
		Vector3 vel = (transform.position - previous) / Time.deltaTime;

		previous = transform.position;
//		if (vel.magnitude > 0f)
//			anim.SetBool("IsMoving", true);
//		else
//			anim.SetBool("IsMoving", false);

		if (vel != new Vector3(0,0,0))
			transform.rotation = Quaternion.LookRotation(vel);

		
	
	}

	public void StartEnterPath()
	{
		iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("spinoPath"), "time", 50f, "easeType", iTween.EaseType.linear, "orienttopath", true, "oncomplete", "StartEnterPath"));
	}

	void StartLeavePath ()
	{
	}

}
