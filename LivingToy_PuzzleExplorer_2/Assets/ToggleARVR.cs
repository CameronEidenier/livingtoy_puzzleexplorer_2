﻿using UnityEngine;
using System.Collections;

public class ToggleARVR : MonoBehaviour {
    public GameObject cardboardMainObject;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void ARVRToggle()
    {
        cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled = !(cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled);
    }
}
