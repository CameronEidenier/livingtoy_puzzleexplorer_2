﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Fog))]
public class FogEditor : Editor 
{

	public override void OnInspectorGUI()
    {
        DrawDefaultInspector();


		Fog myScript = (Fog)target;
        if(GUILayout.Button("Build Fog"))
        {
			myScript.BuildFogSettings();
        }

//		EditorUtility.SetDirty(target);
//		obj.ApplyModifiedProperties();


    }
}
